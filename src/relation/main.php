<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);
// Conexion contra base de datos
try {

    $connec = new Database('api');
    $module=$common->getFileExecute(__DIR__);
    

    /* Bloque relacionado a las Rutas Principales */

    /**
     * @apiService {get} #module/getPixe/{store_id}, Buscar un pixel dentro del registro
     * @apiVersion 0.1.0
     * @apiName getPixe
     * @apiGroup #group
     *
     * @apiParam {Varchar(255)} store_id, Identificador de la tienda a buscar el pixel.
     *
     * @apiRSuccess {Integer} error,  Código 0 conforme todo ha ido bien.
     * @apiRSuccess {Varchar} message,  #MESSAGE_INSETSU
     *
     * @apiRSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "error": 0,
     *       "message": '#MESSAGE_INSETSU'
     *     }
     *
     * @apiRError {Integer} error,  Código 1 no todo ha ido bien.
     * @apiRError {Varchar} message,  #MESSAGE_ERRORSS
     *
     * @apiRErrorExample Error-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "error": 1,
     *       "message":'#MESSAGE_ERRORSS'       
     *     }
     */
    $app -> get('/'.$module.'/getPixel/{store_id}', function($request, $response, $args) use ($connec,$common, $app){
        
        $conn = end($connec);
        $store_id=$args['store_id'];
         
        
        $sql = "SELECT * FROM relation_ps WHERE store_id='$store_id'";
        $query = $conn->prepare($sql);
        $result=$query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        if(count($data)!=0){
            $returnObjects = array('error' => 0, 'message'=>'', 'data'=>$data);  
        }elseif(count($data)==0){
            $returnObjects = array('error' => 0, 'message'=> MESSAGE_LIST_00, 'data'=>$data );
        }else{
            $returnObjects = array('error' => 1, 'message'=> MESSAGE_ERRORSS );
        }
        print_r(json_encode($returnObjects));
    });



/* Bloque relacionado a los Friend tusers_friends*/


} catch(PDOException $e) {
    die(print_r(json_encode(array('error' => 3, 'message'=> "Failed to connect to database ".$e->getMessage()))));
}